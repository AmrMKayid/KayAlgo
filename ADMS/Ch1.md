# 1.7 Exercises

1-1. [3] Show that a + b can be less than min(a, b).
> a+b < min(a,b) ⇔ a < 0 ∧ b < 0 <br>
> a = -3 & b = -4 | a+b = -7 < min(a, b) = -4

1-2. [3] Show that a × b can be less than min(a, b).
> if both the numbers lie between 0 and 1 then ab < min(a,b) <br>
> a = 3 & b = -1 | a*b = -3 < min(a, b) = -1


1-5. [4] The knapsack problem
> a) the first-fit algorithm:	{1,2,5,9,10} => 27 != T <br>
> b) the best-fit algorithm:	{1,2,5,9,10} => 27 != T <br>
> c) largest to smallest:		{10,9,5} => 24 != T <br>

1-6. [5] The set cover problem
> Optimal sets vs greedy repeating numbers in some sets